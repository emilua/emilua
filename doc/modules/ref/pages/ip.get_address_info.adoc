= ip.get_address_info

ifeval::["{doctype}" == "manpage"]

== Name

Emilua - Lua execution engine

endif::[]

== Synopsis

[source,lua]
----
local ip = require "ip"

ip.tcp.get_address_info()
ip.tcp.get_address_v4_info()
ip.tcp.get_address_v6_info()
ip.udp.get_address_info()
ip.udp.get_address_v4_info()
ip.udp.get_address_v6_info()

function(host: string|ip.address, service: string|integer[, flags: string[]])
    -> { address: ip.address, port: integer, canonical_name: string|nil }[]
----

== Description

Forward-resolves host and service into a list of endpoint entries. Current fiber
is suspended until operation finishes.

NOTE: If no `flags` are passed to this function (i.e. `flags` is `nil`) then
this function will follow the glibc defaults even on non-glibc systems:
`bit.bor(address_configured,v4_mapped)`.

Returns a list of entries. Each entry will be a table with the following
members:

* `address: ip.address`.
* `port: integer`.

If `"canonical_name"` is passed in `flags` then each entry will also include:

* `canonical_name: string`.

https://www.boost.org/doc/libs/1_70_0/doc/html/boost_asio/reference/ip__basic_resolver/async_resolve/overload3.html[More
info on Boost.Asio documentation].

If `host` is an `ip.address` then no host name resolution should be attempted.

If `service` is a number then no service name resolution should be attempted.

== Flags

=== `address_configured`

https://www.boost.org/doc/libs/1_70_0/doc/html/boost_asio/reference/ip__resolver_base/address_configured.html[The
flag with same name in Boost.Asio]:

[quote]
____
Only return IPv4 addresses if a non-loopback IPv4 address is configured for the
system. Only return IPv6 addresses if a non-loopback IPv6 address is configured
for the system.
____

=== `all_matching`

https://www.boost.org/doc/libs/1_70_0/doc/html/boost_asio/reference/ip__resolver_base/all_matching.html[The
flag with same name in Boost.Asio]:

[quote]
____
If used with v4_mapped, return all matching IPv6 and IPv4 addresses.
____

=== `canonical_name`

https://www.boost.org/doc/libs/1_70_0/doc/html/boost_asio/reference/ip__resolver_base/canonical_name.html[The
flag with same name in Boost.Asio]:

[quote]
____
Determine the canonical name of the host specified in the query.
____

=== `passive`

https://www.boost.org/doc/libs/1_70_0/doc/html/boost_asio/reference/ip__resolver_base/passive.html[The
flag with same name in Boost.Asio]:

[quote]
____
Indicate that returned endpoint is intended for use as a locally bound socket
endpoint.
____

=== `v4_mapped`

https://www.boost.org/doc/libs/1_70_0/doc/html/boost_asio/reference/ip__resolver_base/v4_mapped.html[The
flag with same name in Boost.Asio]:

[quote]
____
If the query protocol family is specified as IPv6, return IPv4-mapped IPv6
addresses on finding no IPv6 addresses.
____
