#pragma once

#define EMILUA_CONFIG_VERSION_MAJOR @VERSION_MAJOR@
#define EMILUA_CONFIG_VERSION_MINOR @VERSION_MINOR@
#define EMILUA_CONFIG_VERSION_PATCH @VERSION_PATCH@
#define EMILUA_CONFIG_VERSION_STRING @VERSION_STRING@

#define EMILUA_CONFIG_LIBROOTDIR @LIBROOTDIR@

#define EMILUA_CONFIG_ENABLE_PLUGINS @ENABLE_PLUGINS@
#define EMILUA_CONFIG_ENABLE_COLOR @ENABLE_COLOR@
#define EMILUA_CONFIG_ENABLE_FILE_IO @ENABLE_FILE_IO@
#define EMILUA_CONFIG_USE_STANDALONE_ASIO @USE_STANDALONE_ASIO@
#define EMILUA_CONFIG_IPC_ACTOR_MESSAGE_MAX_MEMBERS_NUMBER @IPC_ACTOR_MSG_MAX_MEMBERS_NUMBER@
#define EMILUA_CONFIG_IPC_ACTOR_MAX_CONFIG_MESSAGE_SIZE @IPC_ACTOR_MAX_CONFIG_MSG_SIZE@

// == THREADS AVAILABILITY
//
// 0:: No threading support.
//
// 1 or higher:: Threads available.
//
// == EXECUTION CONTEXTS
//
// 0 or 1:: Only one thread may call io_context::run(). spawn_ctx_threads() will
// always fail with ENOSYS (even if called with 0).
//
// 2 or higher:: Multiple threads may call io_context::run().
// spawn_ctx_threads() fully implemented.
//
// == NOTES
//
// Decreasing this value will change the implementation assumptions and enable a
// few optimizations. Check
// <https://www.boost.org/doc/libs/1_72_0/doc/html/boost_asio/overview/core/concurrency_hint.html>
// for more info.
#define EMILUA_CONFIG_THREAD_SUPPORT_LEVEL @THREAD_SUPPORT_LEVEL@

// On non-zero, one may assume that pthread_sigqueue() (or pthread_kill() on
// Linux) can be used to perform a siglongjmp() (useful to unblock a thread
// blocked on a syscall such as flock()). IOW, there is a sighandler for this
// RT-signal siglongjmp()ing to a thread_local sigjmp_buf.
//
// SA_RESTART is still set on this sighandler. You have to resort to
// siglongjmp() to interrupt whatever was running in the target
// thread. Sanitization already happens in the sighandler using siginfo_t before
// actually doing the siglongjmp() (e.g. ignoring signals sent from unprivileged
// users when we're suid).
#define EMILUA_CONFIG_EINTR_RTSIGNO @EINTR_RTSIGNO@

// ######################
// # FEATURES DETECTION #
// ######################

#define EMILUA_CONFIG_HAVE_PTHREAD_SIGQUEUE @HAVE_PTHREAD_SIGQUEUE@
#define EMILUA_CONFIG_HAVE_RTLD_SET_VAR @HAVE_RTLD_SET_VAR@
