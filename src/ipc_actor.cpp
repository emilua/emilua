// Copyright (c) 2023, 2024 Vinícius dos Santos Oliveira
// SPDX-License-Identifier: MIT OR BSL-1.0

#include <sys/eventfd.h>
#include <sys/mman.h>
#include <sys/wait.h>

#include <iostream>
#include <charconv>

#include <fmt/ostream.h>
#include <fmt/format.h>

#include <boost/preprocessor/stringize.hpp>
#include <boost/nowide/iostream.hpp>
#include <boost/scope_exit.hpp>
#include <boost/hana/less.hpp>
#include <boost/hana/plus.hpp>

#include <cereal/types/vector.hpp>
#include <cereal/types/string.hpp>
#include <cereal/types/unordered_map.hpp>
#include <cereal/archives/binary.hpp>

#include <emilua/proc_set_libc_service.hpp>
#include <emilua/file_descriptor.hpp>
#include <emilua/open_posix_libs.hpp>
#include <emilua/actor.hpp>
#include <emilua/state.hpp>

#if BOOST_OS_LINUX
#include <linux/close_range.h>
#include <linux/seccomp.h>
#include <linux/filter.h>

#include <sys/capability.h>
#include <sys/prctl.h>
#include <grp.h>
#endif // BOOST_OS_LINUX

#if BOOST_OS_BSD_FREE || BOOST_OS_BSD_DRAGONFLY
#include <sys/procctl.h>
#endif // BOOST_OS_BSD_FREE || BOOST_OS_BSD_DRAGONFLY

#if BOOST_OS_BSD_FREE
#include <sys/procdesc.h>

# if !defined(EMILUA_STATIC_BUILD)
#  include <dlfcn.h>
# endif // !defined(EMILUA_STATIC_BUILD)
#endif // BOOST_OS_BSD_FREE

#if EMILUA_CONFIG_ENABLE_PLUGINS && EMILUA_CONFIG_HAVE_RTLD_SET_VAR
#include <link.h>
#endif // EMILUA_CONFIG_ENABLE_PLUGINS && EMILUA_CONFIG_HAVE_RTLD_SET_VAR

#define EMILUA_LUA_HOOK_BUFFER_SIZE (1024 * 1024)
static_assert(EMILUA_LUA_HOOK_BUFFER_SIZE % alignof(std::max_align_t) == 0);

#if !BOOST_OS_BSD_FREE || defined(EMILUA_STATIC_BUILD)
extern char** environ;
#endif // !BOOST_OS_BSD_FREE || defined(EMILUA_STATIC_BUILD)

namespace std::filesystem {
template<class Archive>
void CEREAL_LOAD_MINIMAL_FUNCTION_NAME(
    const Archive&, path& out, const std::string& in)
{
  out = in;
}

template<class Archive>
std::string CEREAL_SAVE_MINIMAL_FUNCTION_NAME(const Archive& ar, const path& p)
{
  return p.string();
}
} // namespace std::filesystem

namespace emilua {

namespace fs = std::filesystem;

static int inboxfd;
static int proc_stdin;
static int proc_stdout;
static int proc_stderr;
static bool proc_stderr_has_color;
static bool has_native_modules_cache;
static bool has_lua_hook;

static std::vector<std::string> environ_buffer1;
static std::vector<char*> environ_buffer2;

struct monotonic_allocator
{
    monotonic_allocator(void* buffer, std::size_t buffer_size)
        : buffer{static_cast<char*>(buffer)}
        , next{static_cast<char*>(buffer)}
        , buffer_size{buffer_size}
    {}

    void* operator()(void* ptr, std::size_t osize, std::size_t nsize)
    {
        static constexpr auto step_size = alignof(std::max_align_t);

        if (nsize == 0)
            return NULL;
        else if (nsize < osize)
            return ptr;

        auto end = buffer + buffer_size;

        if (auto remainder = nsize % step_size ; remainder > 0) {
            auto padding = step_size - remainder;
            nsize += padding;
        }

        if (next + nsize > end) {
            errno = ENOMEM;
            perror("<3>ipc_actor/init/alloc");
            std::exit(1);
        }

        auto ret = next;
        next += nsize;

        if (ptr)
            std::memcpy(ret, ptr, osize);

        return ret;
    }

    char* buffer;
    char* next;
    std::size_t buffer_size;

    static void* alloc(void* ud, void* ptr, std::size_t osize,
                       std::size_t nsize)
    {
        return (*static_cast<monotonic_allocator*>(ud))(ptr, osize, nsize);
    }
};

// may report false negative (e.g. ENOTCAPABLE preventing fstat())
static inline bool is_socket(int fd)
{
    struct stat stfd;
    if (fstat(fd, &stfd) == -1)
        return false;

    return S_ISSOCK(stfd.st_mode);
}

void ipc_actor_inbox_op::do_wait()
{
    service->sock.async_wait(
        asio::socket_base::wait_read,
        asio::bind_executor(
            remap_post_to_defer<strand_type>{executor},
            [self=shared_from_this()](const asio_error_code& ec) {
                self->on_wait(ec);
            }
        )
    );
}

void ipc_actor_inbox_op::on_wait(const asio_error_code& ec)
{
    auto vm_ctx = this->vm_ctx.lock();
    if (!vm_ctx || !vm_ctx->valid())
        return;

    if (!vm_ctx->inbox.open) {
        --vm_ctx->inbox.nsenders;
        vm_ctx->pending_operations.erase(
            vm_ctx->pending_operations.iterator_to(*service));
        delete service;
        return;
    }

    auto recv_fiber = vm_ctx->inbox.recv_fiber;
    if (ec) {
        vm_ctx->pending_operations.erase(
            vm_ctx->pending_operations.iterator_to(*service));
        delete service;
        if (--vm_ctx->inbox.nsenders == 0 && recv_fiber) {
            vm_ctx->inbox.recv_fiber = nullptr;
            vm_ctx->inbox.work_guard.reset();
            vm_ctx->fiber_resume(
                recv_fiber,
                hana::make_set(
                    hana::make_pair(
                        vm_context::options::arguments,
                        hana::make_tuple(errc::no_senders))));
        }
        return;
    }

    struct msghdr msg;
    std::memset(&msg, 0, sizeof(msg));

    struct iovec iov;
    ipc_actor_message message;
    iov.iov_base = &message;
    iov.iov_len = sizeof(message);
    msg.msg_iov = &iov;
    msg.msg_iovlen = 1;

    alignas(cmsghdr) char cmsgbuf[CMSG_SPACE(
        sizeof(int) * EMILUA_CONFIG_IPC_ACTOR_MESSAGE_MAX_MEMBERS_NUMBER)];
    msg.msg_control = cmsgbuf;
    msg.msg_controllen = sizeof(cmsgbuf);

    auto nread = recvmsg(service->sock.native_handle(), &msg, MSG_DONTWAIT);
    if (nread == -1) {
        if (errno == EAGAIN || errno == EWOULDBLOCK) {
            do_wait();
            return;
        }

        vm_ctx->pending_operations.erase(
            vm_ctx->pending_operations.iterator_to(*service));
        delete service;
        if (--vm_ctx->inbox.nsenders == 0 && recv_fiber) {
            vm_ctx->inbox.recv_fiber = nullptr;
            vm_ctx->inbox.work_guard.reset();
            vm_ctx->fiber_resume(
                recv_fiber,
                hana::make_set(
                    hana::make_pair(
                        vm_context::options::arguments,
                        hana::make_tuple(errc::no_senders))));
        }
        return;
    }

    std::vector<int> fds;
    fds.reserve(EMILUA_CONFIG_IPC_ACTOR_MESSAGE_MAX_MEMBERS_NUMBER);
    BOOST_SCOPE_EXIT_ALL(&) {
        for (auto& fd: fds) {
            if (fd != -1) {
                int res = close(fd);
                boost::ignore_unused(res);
            }
        }
    };
    for (struct cmsghdr* cmsg = CMSG_FIRSTHDR(&msg) ; cmsg != NULL ;
         cmsg = CMSG_NXTHDR(&msg, cmsg)) {
        if (cmsg->cmsg_level != SOL_SOCKET || cmsg->cmsg_type != SCM_RIGHTS) {
            continue;
        }

        char* in = (char*)CMSG_DATA(cmsg);
        auto nfds = (cmsg->cmsg_len - CMSG_LEN(0)) / sizeof(int);
        for (std::size_t i = 0 ; i != nfds ; ++i) {
            int fd;
            std::memcpy(&fd, in, sizeof(int));
            in += sizeof(int);
            if (fd != -1)
                fds.emplace_back(fd);
        }
    }

    if (
        nread < static_cast<ssize_t>(sizeof(message.members[0]) * 2) ||
        (msg.msg_flags & (MSG_TRUNC | MSG_CTRUNC))
    ) {
        vm_ctx->pending_operations.erase(
            vm_ctx->pending_operations.iterator_to(*service));
        delete service;
        if (--vm_ctx->inbox.nsenders == 0 && recv_fiber) {
            vm_ctx->inbox.recv_fiber = nullptr;
            vm_ctx->inbox.work_guard.reset();
            vm_ctx->fiber_resume(
                recv_fiber,
                hana::make_set(
                    hana::make_pair(
                        vm_context::options::arguments,
                        hana::make_tuple(errc::no_senders))));
        }
        return;
    }
    service->running = false;

    auto remove_service = [&]() {
        // Ideally we wouldn't close the channel because the assumption is that
        // many writers have this handle and only one is misbehaving. Closing
        // the channel would cut our communication channel with the legitimate
        // parties which really means DoS.
        //
        // Unfortunately a misbehaving writer can just send a 0-sized payload
        // and trick us that EOF was reached anyway. Therefore keeping the
        // connection open just to defend against such DoS attacker would be
        // moot. Let's go ahead and just close the socket already.
        vm_ctx->pending_operations.erase(
            vm_ctx->pending_operations.iterator_to(*service));
        delete service;
        --vm_ctx->inbox.nsenders;
    };

    auto boundsvalid = [&message,&nread](const std::string_view& v) {
        // No overflow ever happens as it's impossible to overrun our buffer
        // with its current encoding scheme. The point is to avoid leaking
        // uninitialized data from our stack. That's the attack we're defending
        // against.
        return v.data() + v.size() <= reinterpret_cast<char*>(&message) + nread;
    };

    if (!recv_fiber) {
        auto& queue = vm_ctx->inbox.incoming;
        queue.emplace_back();

        if (
            message.members[0].as_int ==
            (EXPONENT_MASK | ipc_actor_message::nil)
        ) {
            if (!is_snan(message.members[1].as_int)) {
                queue.back().msg.emplace<lua_Number>(
                    message.members[1].as_double);
                return;
            }

            switch (message.members[1].as_int & MANTISSA_MASK) {
            default:
                remove_service();
                queue.pop_back();
                return;
            case ipc_actor_message::boolean_true:
                queue.back().msg.emplace<bool>(true);
                break;
            case ipc_actor_message::boolean_false:
                queue.back().msg.emplace<bool>(false);
                break;
            case ipc_actor_message::string: {
                std::string_view v(reinterpret_cast<char*>(message.strbuf) + 1,
                                   *message.strbuf);
                if (!boundsvalid(v)) {
                    remove_service();
                    queue.pop_back();
                    return;
                }
                queue.back().msg.emplace<std::string>(v);
                break;
            }
            case ipc_actor_message::file_descriptor: {
                if (fds.size() != 1) {
                    remove_service();
                    queue.pop_back();
                    return;
                }

                queue.back().msg.emplace<
                    std::shared_ptr<inbox_t::file_descriptor_box>
                >(std::make_shared<inbox_t::file_descriptor_box>(fds[0]));
                fds[0] = -1;
                break;
            }
            case ipc_actor_message::actor_address:
                if (fds.size() != 1 || !is_socket(fds[0])) {
                    remove_service();
                    queue.pop_back();
                    return;
                }

                queue.back().msg.emplace<inbox_t::ipc_actor_address>(
                    std::make_shared<inbox_t::file_descriptor_box>(fds[0]));
                fds[0] = -1;
                break;
            }
            return;
        }

        if (nread < static_cast<ssize_t>(sizeof(message.members))) {
            remove_service();
            queue.pop_back();
            return;
        }

        auto nextstr = [strit=message.strbuf]() mutable {
            std::string_view::size_type size = *strit++;
            std::string_view ret(reinterpret_cast<char*>(strit), size);
            strit += size;
            return ret;
        };
        auto& dict = queue.back().msg.emplace<
            std::map<std::string, inbox_t::value_type>>();

        decltype(fds)::size_type fdsidx = 0;
        for (
            int nf = 0 ;
            nf != EMILUA_CONFIG_IPC_ACTOR_MESSAGE_MAX_MEMBERS_NUMBER ;
            ++nf
        ) {
            if (
                message.members[nf].as_int ==
                (EXPONENT_MASK | ipc_actor_message::nil)
            ) {
                assert(nf > 0);
                break;
            }

            auto key = nextstr();
            if (!boundsvalid(key)) {
                remove_service();
                queue.pop_back();
                return;
            }

            if (!is_snan(message.members[nf].as_int)) {
                dict.emplace(key, lua_Number(message.members[nf].as_double));
                continue;
            }

            switch (message.members[nf].as_int & MANTISSA_MASK) {
            default:
                remove_service();
                queue.pop_back();
                return;
            case ipc_actor_message::boolean_true:
                dict.emplace(key, true);
                break;
            case ipc_actor_message::boolean_false:
                dict.emplace(key, false);
                break;
            case ipc_actor_message::string: {
                auto value = nextstr();
                if (!boundsvalid(value)) {
                    remove_service();
                    queue.pop_back();
                    return;
                }
                dict.emplace(key, static_cast<std::string>(value));
                break;
            }
            case ipc_actor_message::file_descriptor:
                if (fdsidx == fds.size()) {
                    remove_service();
                    queue.pop_back();
                    return;
                }

                dict.emplace(
                    key,
                    std::make_shared<inbox_t::file_descriptor_box>(
                        fds[fdsidx]));
                fds[fdsidx++] = -1;
                break;
            case ipc_actor_message::actor_address:
                if (fdsidx == fds.size() || !is_socket(fds[fdsidx])) {
                    remove_service();
                    queue.pop_back();
                    return;
                }

                dict.emplace(
                    key,
                    inbox_t::ipc_actor_address{
                        std::make_shared<inbox_t::file_descriptor_box>(
                            fds[fdsidx])});
                fds[fdsidx++] = -1;
                break;
            }
        }
        return;
    }

    struct bad_message_t {};

    auto checkbounds = [&boundsvalid](const std::string_view& v) {
        if (!boundsvalid(v)) {
            throw bad_message_t{};
        }
    };

    auto deserializer = [&message,&checkbounds,&fds,nread,this](
        lua_State* recv_fiber
    ) {
        if (
            message.members[0].as_int ==
            (EXPONENT_MASK | ipc_actor_message::nil)
        ) {
            if (!is_snan(message.members[1].as_int)) {
                lua_pushnumber(recv_fiber, message.members[1].as_double);
                return;
            }

            switch (message.members[1].as_int & MANTISSA_MASK) {
            default:
                throw bad_message_t{};
            case ipc_actor_message::boolean_true:
                lua_pushboolean(recv_fiber, 1);
                break;
            case ipc_actor_message::boolean_false:
                lua_pushboolean(recv_fiber, 0);
                break;
            case ipc_actor_message::string: {
                std::string_view v(reinterpret_cast<char*>(message.strbuf) + 1,
                                   *message.strbuf);
                checkbounds(v);
                push(recv_fiber, v);
                break;
            }
            case ipc_actor_message::file_descriptor: {
                if (fds.size() != 1) {
                    throw bad_message_t{};
                }

                auto fdhandle = static_cast<file_descriptor_handle*>(
                    lua_newuserdata(recv_fiber, sizeof(file_descriptor_handle))
                );
                rawgetp(recv_fiber, LUA_REGISTRYINDEX, &file_descriptor_mt_key);
                setmetatable(recv_fiber, -2);
                *fdhandle = fds[0];
                fds[0] = -1;
                break;
            }
            case ipc_actor_message::actor_address:
                if (fds.size() != 1 || !is_socket(fds[0])) {
                    throw bad_message_t{};
                }

                auto ch = static_cast<ipc_actor_address*>(
                    lua_newuserdata(recv_fiber, sizeof(ipc_actor_address))
                );
                rawgetp(recv_fiber, LUA_REGISTRYINDEX, &ipc_actor_chan_mt_key);
                setmetatable(recv_fiber, -2);
                new (ch) ipc_actor_address{executor.context()};
                {
                    asio::local::seq_packet_protocol protocol;
                    asio_error_code ignored_ec;
                    ch->dest.assign(protocol, fds[0], ignored_ec);
                    assert(!ignored_ec);
                    fds[0] = -1;
                }
                break;
            }
            return;
        }

        if (nread < static_cast<ssize_t>(sizeof(message.members))) {
            throw bad_message_t{};
        }

        auto nextstr = [strit=message.strbuf,&checkbounds]() mutable {
            std::string_view::size_type size = *strit++;
            std::string_view ret(reinterpret_cast<char*>(strit), size);
            strit += size;
            checkbounds(ret);
            return ret;
        };
        lua_newtable(recv_fiber);

        decltype(fds)::size_type fdsidx = 0;
        for (
            int nf = 0 ;
            nf != EMILUA_CONFIG_IPC_ACTOR_MESSAGE_MAX_MEMBERS_NUMBER ;
            ++nf
        ) {
            if (
                message.members[nf].as_int ==
                (EXPONENT_MASK | ipc_actor_message::nil)
            ) {
                assert(nf > 0);
                break;
            }

            auto key = nextstr();
            push(recv_fiber, key);

            if (!is_snan(message.members[nf].as_int)) {
                lua_pushnumber(recv_fiber, message.members[nf].as_double);
                lua_rawset(recv_fiber, -3);
                continue;
            }

            switch (message.members[nf].as_int & MANTISSA_MASK) {
            default:
                throw bad_message_t{};
            case ipc_actor_message::boolean_true:
                lua_pushboolean(recv_fiber, 1);
                break;
            case ipc_actor_message::boolean_false:
                lua_pushboolean(recv_fiber, 0);
                break;
            case ipc_actor_message::string:
                push(recv_fiber, nextstr());
                break;
            case ipc_actor_message::file_descriptor: {
                if (fdsidx == fds.size()) {
                    throw bad_message_t{};
                }

                auto fdhandle = static_cast<file_descriptor_handle*>(
                    lua_newuserdata(recv_fiber, sizeof(file_descriptor_handle))
                );
                rawgetp(recv_fiber, LUA_REGISTRYINDEX, &file_descriptor_mt_key);
                setmetatable(recv_fiber, -2);
                *fdhandle = fds[fdsidx];
                fds[fdsidx++] = -1;
                break;
            }
            case ipc_actor_message::actor_address:
                if (fdsidx == fds.size() || !is_socket(fds[fdsidx])) {
                    throw bad_message_t{};
                }

                auto ch = static_cast<ipc_actor_address*>(
                    lua_newuserdata(recv_fiber, sizeof(ipc_actor_address))
                );
                rawgetp(recv_fiber, LUA_REGISTRYINDEX, &ipc_actor_chan_mt_key);
                setmetatable(recv_fiber, -2);
                new (ch) ipc_actor_address{executor.context()};
                {
                    asio::local::seq_packet_protocol protocol;
                    asio_error_code ignored_ec;
                    ch->dest.assign(protocol, fds[fdsidx], ignored_ec);
                    assert(!ignored_ec);
                    fds[fdsidx++] = -1;
                }
                break;
            }
            lua_rawset(recv_fiber, -3);
        }
    };

    vm_ctx->inbox.recv_fiber = nullptr;
    vm_ctx->inbox.work_guard.reset();
    try {
        vm_ctx->fiber_resume(
            recv_fiber,
            hana::make_set(
                hana::make_pair(
                    vm_context::options::arguments,
                    hana::make_tuple(std::nullopt, deserializer))));
    } catch (const bad_message_t&) {
        remove_service();
        if (vm_ctx->inbox.nsenders == 0) {
            vm_ctx->fiber_resume(
                recv_fiber,
                hana::make_set(
                    hana::make_pair(
                        vm_context::options::arguments,
                        hana::make_tuple(errc::no_senders))));
        } else {
            vm_ctx->inbox.recv_fiber = recv_fiber;
            vm_ctx->inbox.work_guard = vm_ctx;
        }
    }
}

static int child_main(void*)
{
    switch (proc_stdin) {
    case -1: {
        int pipefd[2];
        if (pipe(pipefd) == -1)
            return 1;
        if (dup2(pipefd[0], STDIN_FILENO) == -1)
            return 1;
        break;
    }
    case STDIN_FILENO:
        break;
    default:
        if (dup2(proc_stdin, STDIN_FILENO) == -1)
            return 1;
    }

    switch (proc_stdout) {
    case -1: {
        int pipefd[2];
        if (pipe(pipefd) == -1)
            return 1;
        if (dup2(pipefd[1], STDOUT_FILENO) == -1)
            return 1;
        break;
    }
    case STDOUT_FILENO:
        break;
    default:
        if (dup2(proc_stdout, STDOUT_FILENO) == -1)
            return 1;
    }

    switch (proc_stderr) {
    case -1: {
        int pipefd[2];
        if (pipe(pipefd) == -1)
            return 1;
        if (dup2(pipefd[1], STDERR_FILENO) == -1)
            return 1;
        break;
    }
    case STDERR_FILENO:
        break;
    default:
        if (dup2(proc_stderr, STDERR_FILENO) == -1)
            return 1;
    }

    if (dup2(inboxfd, 3) == -1)
        return 1;
    inboxfd = 3;

    {
        int oldflags = fcntl(inboxfd, F_GETFD);
        assert(oldflags != -1);
        if (fcntl(inboxfd, F_SETFD, oldflags | FD_CLOEXEC) == -1)
            return 1;
    }

    if (close_range(4, UINT_MAX, /*flags=*/0) == -1)
        return 1;

    {
        struct sigaction sa;
        sa.sa_handler = SIG_DFL;
        sigemptyset(&sa.sa_mask);
        // buggy CLONE_CLEAR_SIGHAND won't clear sa_flags so we do it manually
        sa.sa_flags = 0;
        sigaction(SIGCHLD, /*act=*/&sa, /*oldact=*/NULL);
        sigaction(SIGPIPE, /*act=*/&sa, /*oldact=*/NULL);

        sigset_t set;
        sigfillset(&set);
        sigdelset(&set, SIGCHLD);
        sigprocmask(SIG_UNBLOCK, &set, /*oldset=*/NULL);
    }

    // The parent-death signal is a completely voluntary ritual and an untrusted
    // guest can just disable it later on. However we don't depend on
    // parent-death signals. We install a parent-death signal merely as a
    // convenience for bad sysadmins that forcefully kill the supervisor
    // processor (an unsafe operation in itself). The parent-death signal
    // setting is also cleared upon changes to effective user ID, effective
    // group ID, filesystem user ID, or filesystem group ID.
#if BOOST_OS_LINUX
    prctl(PR_SET_PDEATHSIG, SIGKILL);
#elif BOOST_OS_BSD_FREE || BOOST_OS_BSD_DRAGONFLY
    {
        int signo = SIGKILL;
        procctl(P_PID, 0, PROC_PDEATHSIG_CTL, &signo);
    }
#else
# error "OS not supported"
#endif // BOOST_OS_LINUX

    std::string buffer;
    buffer.resize(EMILUA_CONFIG_IPC_ACTOR_MAX_CONFIG_MESSAGE_SIZE);

    {
        auto nread = read(inboxfd, buffer.data(), buffer.size());
        if (nread == -1 || nread == 0)
            return 1;

        // the supervisor process writes in the same fd so we can only shutdown
        // it upon reaching the sync point which happens to be the receipt of
        // the first message
        if (shutdown(inboxfd, SHUT_WR) == -1)
            return 1;

        buffer.resize(nread);
    }

    if (has_native_modules_cache) {
        struct msghdr msg;
        std::memset(&msg, 0, sizeof(msg));

        char buf[1];

        struct iovec iov;
        iov.iov_base = buf;
        iov.iov_len = 1;
        msg.msg_iov = &iov;
        msg.msg_iovlen = 1;

        alignas(cmsghdr) char cmsgbuf[CMSG_SPACE(sizeof(int))];
        msg.msg_control = cmsgbuf;
        msg.msg_controllen = sizeof(cmsgbuf);

        auto nread = recvmsg(inboxfd, &msg, MSG_CMSG_CLOEXEC);
        if (
            nread == -1 || nread == 0 ||
            (msg.msg_flags & (MSG_TRUNC | MSG_CTRUNC))
        ) {
            return 1;
        }

        int fdarg = -1;
        for (struct cmsghdr* cmsg = CMSG_FIRSTHDR(&msg) ; cmsg != NULL ;
             cmsg = CMSG_NXTHDR(&msg, cmsg)) {
            if (cmsg->cmsg_level != SOL_SOCKET || cmsg->cmsg_type != SCM_RIGHTS)
                continue;

            std::memcpy(&fdarg, CMSG_DATA(cmsg), sizeof(int));
            break;
        }
        assert(fdarg == 4);
    }

    if (has_lua_hook) {
        monotonic_allocator allocator{
            malloc(EMILUA_LUA_HOOK_BUFFER_SIZE), EMILUA_LUA_HOOK_BUFFER_SIZE};
        BOOST_SCOPE_EXIT_ALL(&) {
            explicit_bzero(allocator.buffer, allocator.next - allocator.buffer);
            free(allocator.buffer);
        };

        struct msghdr msg;
        std::memset(&msg, 0, sizeof(msg));

        struct iovec iov;
        iov.iov_base = allocator.buffer;
        iov.iov_len = allocator.buffer_size;
        msg.msg_iov = &iov;
        msg.msg_iovlen = 1;

        alignas(cmsghdr) char cmsgbuf[CMSG_SPACE(sizeof(int))];
        msg.msg_control = cmsgbuf;
        msg.msg_controllen = sizeof(cmsgbuf);

        auto nread = recvmsg(inboxfd, &msg, MSG_CMSG_CLOEXEC);
        if (
            nread == -1 || nread == 0 ||
            (msg.msg_flags & (MSG_TRUNC | MSG_CTRUNC)) ||
            allocator(NULL, 0, nread) == NULL ||
            allocator.next == allocator.buffer + allocator.buffer_size
        ) {
            return 1;
        }

        int fdarg = -1;
        for (struct cmsghdr* cmsg = CMSG_FIRSTHDR(&msg) ; cmsg != NULL ;
             cmsg = CMSG_NXTHDR(&msg, cmsg)) {
            if (cmsg->cmsg_level != SOL_SOCKET || cmsg->cmsg_type != SCM_RIGHTS)
                continue;

            std::memcpy(&fdarg, CMSG_DATA(cmsg), sizeof(int));
            break;
        }

        lua_State* L = lua_newstate(allocator.alloc, &allocator);
        BOOST_SCOPE_EXIT_ALL(&) { lua_close(L); };
        lua_gc(L, LUA_GCSTOP, /*unused_data=*/0);
        luaL_openlibs(L);
        open_posix_libs(L);

        if (fdarg != -1) {
            lua_pushinteger(L, fdarg);
            lua_setfield(L, LUA_GLOBALSINDEX, "arg");
        }

        if (luaL_loadbuffer(L, allocator.buffer, nread, "@init.script") != 0) {
            const char* errstr = "unknown error";
            if (lua_isstring(L, -1)) {
                errstr = lua_tostring(L, -1);
            }
            fprintf(stderr, "Failed to load Lua chunk: %s\n", errstr);
            return 1;
        }
        if (lua_pcall(L, /*nargs=*/0, /*nresults=*/0, /*errfunc=*/0) != 0) {
            if (lua_type(L, -1) == LUA_TSTRING) {
                auto prefix = "<3>ipc_actor/init/script: "sv;
                auto from_lua = tostringview(L);
                std::string error_string;
                error_string.reserve(prefix.size() + from_lua.size() + 1);
                error_string += prefix;
                error_string += from_lua;
                error_string += '\n';
                try {
                    std::cerr << error_string;
                } catch (const std::ios_base::failure&) {}
            }
            return 1;
        }

        if (has_native_modules_cache) {
            if (close_range(5, UINT_MAX, /*flags=*/0) == -1)
                return 1;
        } else {
            if (close_range(4, UINT_MAX, /*flags=*/0) == -1)
                return 1;
        }
    }

    if (getpid() == 1) {
        int evfd = eventfd(0, EFD_SEMAPHORE);
        if (evfd == -1)
            return 1;
        auto atfork_parent = [&buffer,&evfd]() -> std::optional<int> {
            explicit_bzero(buffer.data(), buffer.size());
#if BOOST_OS_LINUX
            if (prctl(PR_SET_DUMPABLE, 0) == -1) {
                return 1;
            }
#elif BOOST_OS_BSD_FREE
            if (
                int val = PROC_TRACE_CTL_DISABLE ;
                procctl(P_PID, 0, PROC_TRACE_CTL, &val) == -1
            ) {
                return 1;
            }
#else
# error "OS not supported"
#endif // BOOST_OS_LINUX
            if (eventfd_write(evfd, 1) == -1)
                return 1;

            return std::nullopt;
        };

        auto exit_code = app_context::handle_pid1(atfork_parent);
        if (exit_code)
            return *exit_code;

        eventfd_t evval;
        if (eventfd_read(evfd, &evval) == -1)
            return 1;
        close(evfd);
    }

    {
        struct sigaction sa;
        sa.sa_handler = SIG_IGN;
        sigemptyset(&sa.sa_mask);
        sa.sa_flags = 0;

        sigaction(SIGPIPE, /*act=*/&sa, /*oldact=*/NULL);
    }

    int ipc_actor_service_pipe[2];
    if (socketpair(AF_UNIX, SOCK_SEQPACKET, 0, ipc_actor_service_pipe) == -1) {
        ipc_actor_service_pipe[0] = -1;
        ipc_actor_service_pipe[1] = -1;
        perror("<4>Failed to start subprocess-based actor subsystem");
    }

    if (ipc_actor_service_pipe[0] != -1) {
        shutdown(ipc_actor_service_pipe[0], SHUT_WR);
        shutdown(ipc_actor_service_pipe[1], SHUT_RD);

        switch (fork()) {
        case -1: {
            perror("<4>Failed to start subprocess-based actor subsystem");
            close(ipc_actor_service_pipe[0]);
            close(ipc_actor_service_pipe[1]);
            ipc_actor_service_pipe[0] = -1;
            ipc_actor_service_pipe[1] = -1;
            break;
        }
        case 0:
            close(ipc_actor_service_pipe[1]);
            explicit_bzero(buffer.data(), buffer.size());
            buffer.clear();
            buffer.shrink_to_fit();
            return emilua::app_context::ipc_actor_service_main(
                ipc_actor_service_pipe[0]);
        default: {
            close(ipc_actor_service_pipe[0]);
            ipc_actor_service_pipe[0] = -1;

            emilua::bzero_region args;
            args.s = nullptr;
            args.n = 0;
            write(ipc_actor_service_pipe[1], &args, sizeof(args));
        }
        }
    }

    if (EMILUA_CONFIG_EINTR_RTSIGNO != 0) {
        struct sigaction sa;
        std::memset(&sa, 0, sizeof(struct sigaction));

        // from SIG_UNBLOCK to SIG_BLOCK, execution MUST only use
        // async-signal-safe code
        sigemptyset(&sa.sa_mask);
        sigaddset(&sa.sa_mask, EMILUA_CONFIG_EINTR_RTSIGNO);
        sigprocmask(SIG_BLOCK, &sa.sa_mask, /*oldset=*/NULL);

        sa.sa_sigaction = emilua::longjmp_on_rtsigno;
        sa.sa_flags = SA_RESTART | SA_SIGINFO;
        sigaction(EMILUA_CONFIG_EINTR_RTSIGNO, /*act=*/&sa, /*oldact=*/NULL);
    }

    int main_ctx_concurrency_hint;
    fs::path entry_point;
    fs::path import_root;

    app_context appctx;
    appctx.app_args.reserve(2);
    appctx.app_args.emplace_back();
    appctx.ipc_actor_service_sockfd = ipc_actor_service_pipe[1];

    int libc_service_sockfd = -1;

    while (has_native_modules_cache) {
        struct msghdr msg;
        std::memset(&msg, 0, sizeof(msg));

        std::array<char, 256 + /*sentinel_sz=*/1> buf;

        struct iovec iov;
        iov.iov_base = buf.data();
        iov.iov_len = buf.size() - 1; //< the extra byte will be used for '\0'
        msg.msg_iov = &iov;
        msg.msg_iovlen = 1;

        alignas(cmsghdr) char cmsgbuf[CMSG_SPACE(sizeof(int))];
        msg.msg_control = cmsgbuf;
        msg.msg_controllen = sizeof(cmsgbuf);

        auto nread = recvmsg(4, &msg, MSG_CMSG_CLOEXEC);
        if (
            nread == -1 || nread == 0 ||
            (msg.msg_flags & (MSG_TRUNC | MSG_CTRUNC))
        ) {
            return 1;
        }
        buf[nread] = '\0'; //< doesn't overflow because extraneous alloc'ed byte

        int fdarg = -1;
        for (struct cmsghdr* cmsg = CMSG_FIRSTHDR(&msg) ; cmsg != NULL ;
             cmsg = CMSG_NXTHDR(&msg, cmsg)) {
            if (cmsg->cmsg_level != SOL_SOCKET || cmsg->cmsg_type != SCM_RIGHTS)
                continue;

            std::memcpy(&fdarg, CMSG_DATA(cmsg), sizeof(int));
            break;
        }

        switch (buf[0]) {
        default:
            assert(false);
        case ipc_actor_start_vm_request::PRELOAD_EOF:
            assert(fdarg == -1);
            close(4);
            has_native_modules_cache = false;
            break;
#if EMILUA_CONFIG_ENABLE_PLUGINS
        case ipc_actor_start_vm_request::PRELOAD_FILE:
            assert(fdarg != -1);
            appctx.native_modules_file_preload.emplace(buf.data() + 1, fdarg);
            break;
        case ipc_actor_start_vm_request::PRELOAD_DIR:
            assert(fdarg != -1);
            appctx.native_modules_dir_preload.emplace_back(fdarg);
            break;
# if EMILUA_CONFIG_HAVE_RTLD_SET_VAR
        case ipc_actor_start_vm_request::PRELOAD_LD_LIBRARY_DIRECTORY:
            assert(fdarg != -1);
            appctx.ld_library_directories.emplace_back(fdarg);
            break;
# endif // EMILUA_CONFIG_HAVE_RTLD_SET_VAR
#endif // EMILUA_CONFIG_ENABLE_PLUGINS
        case ipc_actor_start_vm_request::PRELOAD_LIBC_SERVICE:
            assert(fdarg != -1);
            assert(libc_service_sockfd == -1);
            libc_service_sockfd = fdarg;
            break;
        }
    }

#if EMILUA_CONFIG_ENABLE_PLUGINS && EMILUA_CONFIG_HAVE_RTLD_SET_VAR
    if (appctx.ld_library_directories.size() > 0) {
        // rtld_set_var() calls xstrdup() on our value so it's safe to dealloc
        // this buffer after we're done
        std::string value;

        auto it = appctx.ld_library_directories.begin();
        value += std::to_string(*it);
        for (++it ; it != appctx.ld_library_directories.end() ; ++it) {
            value += ':' + std::to_string(*it);
        }

        if (rtld_set_var("LIBRARY_PATH_FDS", value.c_str()) != 0) {
            for (int fd : appctx.ld_library_directories) {
                std::ignore = close(fd);
            }
            appctx.ld_library_directories.clear();
        }
    }
#endif // EMILUA_CONFIG_ENABLE_PLUGINS && EMILUA_CONFIG_HAVE_RTLD_SET_VAR

    {
        std::istringstream is{buffer};
        is.imbue(std::locale::classic());
        cereal::BinaryInputArchive ia{is};

        std::string str;

        ia >> main_ctx_concurrency_hint;

        ia >> str;
        entry_point = fs::path{str, fs::path::native_format};
        appctx.app_args.emplace_back(entry_point.c_str());
        str.clear();

        ia >> str;
        if (str.size() > 0) {
            import_root = fs::path{str, fs::path::native_format};
        }
        str.clear();

        ia >> environ_buffer1;
        environ_buffer2.reserve(environ_buffer1.size() + 1);
        for (auto& s : environ_buffer1) {
            if (!s.starts_with('\0')) {
                environ_buffer2.emplace_back(s.data());
            }
            auto idx = s.find('=');
            std::string_view s2{s};
            appctx.app_env.emplace(s2.substr(0, idx), s2.substr(idx + 1));
        }
        environ_buffer2.emplace_back(nullptr);
#if !BOOST_OS_BSD_FREE || defined(EMILUA_STATIC_BUILD)
        environ = environ_buffer2.data();
#else // !BOOST_OS_BSD_FREE || defined(EMILUA_STATIC_BUILD)
        char*** environp = static_cast<char***>(dlsym(RTLD_DEFAULT, "environ"));
        *environp = environ_buffer2.data();
#endif // !BOOST_OS_BSD_FREE || defined(EMILUA_STATIC_BUILD)

        ia >> appctx.modules_cache_registry;

        if (libc_service_sockfd != -1) {
            std::map<int, std::string> libc_service_lua_filters;
            ia >> libc_service_lua_filters;
            libc_service::proc_set(
                libc_service_sockfd, std::move(libc_service_lua_filters));
        }
    }
    buffer.clear();
    buffer.shrink_to_fit();

    try {
        std::locale native_locale{""};
        std::locale::global(native_locale);
        std::cin.imbue(native_locale);
        std::cout.imbue(native_locale);
        std::cerr.imbue(native_locale);
        std::clog.imbue(native_locale);
    } catch (const std::exception& e) {
        try {
            fmt::print(
                boost::nowide::cerr,
                FMT_STRING("<4>Failed to set the native locale: `{}`\n"),
                e.what());
        } catch (const std::ios_base::failure&) {}
    }

    emilua::stdout_has_color = [&appctx]() {
        if (
            auto it = appctx.app_env.find("EMILUA_COLORS") ;
            it != appctx.app_env.end()
        ) {
            if (it->second == "1") {
                return true;
            } else if (it->second == "0") {
                return false;
            } else if (it->second.size() > 0) {
                try {
                    std::cerr <<
                        "<4>Ignoring unrecognized value for EMILUA_COLORS\n";
                } catch (const std::ios_base::failure&) {}
            }
        }

        if (proc_stderr == STDERR_FILENO)
            return static_cast<bool>(proc_stderr_has_color);

        return false;
    }();

    if (
        auto it = appctx.app_env.find("EMILUA_LOG_LEVELS") ;
        it != appctx.app_env.end()
    ) {
        std::string_view env = it->second;
        int level;
        auto res = std::from_chars(
            env.data(), env.data() + env.size(), level);
        if (res.ec == std::errc{})
            emilua::log_domain<emilua::default_log_domain>::log_level = level;
    }

    if (
        auto it = appctx.app_env.find("EMILUA_PATH") ;
        it != appctx.app_env.end()
    ) {
        for (std::string_view spec{it->second} ;;) {
            std::string_view::size_type sepidx = spec.find(':');
            if (sepidx == std::string_view::npos) {
                appctx.emilua_path.emplace_back(spec, fs::path::native_format);
                break;
            } else {
                appctx.emilua_path.emplace_back(
                    spec.substr(0, sepidx), fs::path::native_format);
                spec.remove_prefix(sepidx + 1);
            }
        }
    }

    {
        const std::unique_lock wlock{appctx.modules_cache_registry_mtx};
        create_native_modules(wlock, appctx);
    }

    {
        asio::io_context ioctx{main_ctx_concurrency_hint};
        asio::make_service<properties_service>(
            ioctx, main_ctx_concurrency_hint);

        try {
            auto vm_ctx = make_vm(
                ioctx, appctx, ContextType::worker, entry_point, import_root);
            appctx.master_vm = vm_ctx;

            ++vm_ctx->inbox.nsenders;
            auto inbox_service = new ipc_actor_inbox_service{ioctx, inboxfd};
            vm_ctx->pending_operations.push_back(*inbox_service);

            vm_ctx->strand().post([vm_ctx]() {
                vm_ctx->fiber_resume(
                    vm_ctx->L(),
                    hana::make_set(
                        vm_context::options::skip_clear_interrupter));
            }, std::allocator<void>{});
        } catch (const std::exception& e) {
            try {
                std::cerr << "Error starting the lua VM: " << e.what() <<
                    std::endl;
            } catch (const std::ios_base::failure&) {}
            return 1;
        }

        ioctx.run();
    }

    {
        std::unique_lock<std::mutex> lk{appctx.extra_threads_count_mtx};
        while (appctx.extra_threads_count > 0)
            appctx.extra_threads_count_empty_cond.wait(lk);
    }

    destroy_native_modules();

    try {
        // The glibc runtime won't flush `stdout` on clone()d processes because
        // doing so is generally unsafe since buffered data would then be
        // flushed twice. However the IO buffers were empty at the time we were
        // forked to eventually arrive here, so manually flushing is safe here
        // (and if we don't flush, the glibc runtime won't... which would be a
        // bug).
        std::cout << std::flush;
    } catch (const std::ios_base::failure&) {}

    return appctx.exit_code;
}

std::optional<int> app_context::handle_pid1(
    std::function<std::optional<int>()> atfork_on_parent)
{
    assert(getpid() == 1);

    static pid_t childpid;

    static constexpr auto sighandler = [](int signo) {
        // upon reaping childpid, there's a small window for an ESRCH
        // race... and that doesn't matter because as soon as child finishes the
        // whole pidns is going down
        kill(childpid, signo);
    };

    switch (childpid = fork()) {
    case -1:
        return 1;
    case 0:
        return std::nullopt;
    default: {
        struct sigaction sa;
        sa.sa_handler = sighandler;
        sigemptyset(&sa.sa_mask);
        sa.sa_flags = SA_RESTART;

        // It'd be futile to re-route every signal (e.g. SIGSTOP) so we don't
        // even try it. Only re-route signals that are useful for parent-child
        // communication.
        sigaction(SIGTERM, /*act=*/&sa, /*oldact=*/NULL);
        sigaction(SIGUSR1, /*act=*/&sa, /*oldact=*/NULL);
        sigaction(SIGUSR2, /*act=*/&sa, /*oldact=*/NULL);

        // Applications that don't require a controlling terminal, such as
        // daemons, usually re-purpose SIGHUP as a signal to re-read
        // configuration files.
        sigaction(SIGHUP, /*act=*/&sa, /*oldact=*/NULL);

        // SIGINT shouldn't be the first choice for parent-child
        // communication. SIGINT is generated by the TTY driver itself and as
        // such means UI running in the interested process communicated an
        // UI-initiated interruption request (e.g. confirmation dialogues are
        // allowed). However we also re-route SIGINT because
        // LINUX_REBOOT_CMD_CAD_OFF (again: an UI interaction) will send SIGINT
        // to PID1.
        sigaction(SIGINT, /*act=*/&sa, /*oldact=*/NULL);

        // SysVinit 3.10 was released with a change to handle SIGRTMIN+4. The
        // change was motivated by systemD's machinectl behavior. Here we just
        // follow the same trend.
        //
        // Given SIGRTMIN+4 is a RT signal, we should in theory be using
        // sigqueue() instead of kill() in the sighandler and handle EAGAIN to
        // avoid signal coalescing. However it's acceptable for the behavior
        // desired here (poweroff.target) to coalesce as it's an one-time action
        // anyway.
        sigaction(SIGRTMIN+4, /*act=*/&sa, /*oldact=*/NULL);

        // We only call atfork_on_parent after sighandling registration finishes
        // so it's possible to synchronize child actions that depend on
        // sighandling being ready. An example of such sync needs may be found
        // in systemD's sd_notify()/X_SYSTEMD_SIGNALS_LEVEL=2.
        if (atfork_on_parent) {
            if (auto exit_code = atfork_on_parent() ; exit_code)
                return *exit_code;
            atfork_on_parent = nullptr;
        }

        // Allow EPIPE to propagate if child process closes standard file
        // descriptors.
        close_range(0, UINT_MAX, /*flags=*/0);

        for (siginfo_t info ;;) {
            waitid(P_ALL, /*ignored_id=*/0, &info, WEXITED);
            if (info.si_pid == childpid) {
                if (info.si_code == CLD_EXITED) {
                    return info.si_status;
                } else {
                    // as in bash, add 128 to the signal number
                    return 128 + info.si_status;
                }
            }
        }
    }
    }
}

int app_context::ipc_actor_service_main(int sockfd)
{
    for (;;) {
        bzero_region args;
        auto nread = read(sockfd, &args, sizeof(args));
        if (nread == -1 || nread == 0)
            return 1;

        assert(nread == sizeof(args));
        if (args.s == nullptr)
            break;

        explicit_bzero(args.s, args.n);
    }
    {
        // we don't use clearenv() because it's unsafe when we manipulate
        // environ directly
        static char* emptyenv[1] = { NULL };
#if !BOOST_OS_BSD_FREE || defined(EMILUA_STATIC_BUILD)
        environ = emptyenv;
#else // !BOOST_OS_BSD_FREE || defined(EMILUA_STATIC_BUILD)
        char*** environp = static_cast<char***>(dlsym(RTLD_DEFAULT, "environ"));
        *environp = emptyenv;
#endif // !BOOST_OS_BSD_FREE || defined(EMILUA_STATIC_BUILD)
    }

    if (dup2(sockfd, 3) == -1) {
        perror("<3>ipc_actor/supervisor");
        return 1;
    }
    sockfd = 3;

    if (close_range(4, UINT_MAX, /*flags=*/0) == -1) {
        perror("<3>ipc_actor/supervisor");
        return 1;
    }

    {
        struct sigaction sa;
        sa.sa_handler = SIG_DFL;
        sigemptyset(&sa.sa_mask);
        sa.sa_flags = SA_NOCLDWAIT;
        sigaction(SIGCHLD, /*act=*/&sa, /*oldact=*/NULL);

        // The only real concern is to ensure this process image
        // (/proc/self/exe) will stay ETXTBSY for as long as we have any
        // (untrusted) children.
        sigset_t set;
        sigfillset(&set);
        sigdelset(&set, SIGCHLD);
        sigprocmask(SIG_BLOCK, &set, /*oldset=*/NULL);
    }

    umask(S_IWGRP | S_IWOTH);

    struct msghdr msg;

    // That's the only acceptable data in the supervisor process memory (file
    // descriptor numbers/actions). The supervisor process memory is cloned to
    // subsequent children and we don't want sensitive data leaking into
    // them. To avoid even loading any sensitive data in the supervisor process
    // memory we serialize and send directly to the children processes instead.
    ipc_actor_start_vm_request request;
    struct iovec iov;

    alignas(cmsghdr) char cmsgbuf[CMSG_SPACE(sizeof(int) * 4)];

    // WARNING: Any exit-path now must wait for all children to finish!
    for (;;) {
        std::memset(&msg, 0, sizeof(msg));
        iov.iov_base = &request;
        iov.iov_len = sizeof(request);
        msg.msg_iov = &iov;
        msg.msg_iovlen = 1;
        msg.msg_control = cmsgbuf;
        msg.msg_controllen = sizeof(cmsgbuf);

        auto nread = recvmsg(sockfd, &msg, /*flags=*/0);
        switch (nread) {
        case -1:
            perror("<3>ipc_actor/supervisor");
            goto out_cleanup_and_return_failure;
        case 0:
            goto out_cleanup;
        }
        assert(nread == sizeof(request));

        switch (request.type) {
        case ipc_actor_start_vm_request::SETRESUID: {
            int pout;
            char buf[1];

            struct cmsghdr* cmsg = CMSG_FIRSTHDR(&msg);
            assert(cmsg->cmsg_level == SOL_SOCKET &&
                   cmsg->cmsg_type == SCM_RIGHTS);
            assert(sizeof(int) == cmsg->cmsg_len - CMSG_LEN(0));
            std::memcpy(&pout, CMSG_DATA(cmsg), sizeof(int));

            if (setresuid(
                request.resuid[0], request.resuid[1], request.resuid[2]
            ) == -1) {
                goto out_cleanup_and_return_failure;
            }
            write(pout, buf, 1);
            close(pout);
            continue;
        }
        case ipc_actor_start_vm_request::SETRESGID: {
            int pout;
            char buf[1];

            struct cmsghdr* cmsg = CMSG_FIRSTHDR(&msg);
            assert(cmsg->cmsg_level == SOL_SOCKET &&
                   cmsg->cmsg_type == SCM_RIGHTS);
            assert(sizeof(int) == cmsg->cmsg_len - CMSG_LEN(0));
            std::memcpy(&pout, CMSG_DATA(cmsg), sizeof(int));

            if (setresgid(
                request.resgid[0], request.resgid[1], request.resgid[2]
            ) == -1) {
                goto out_cleanup_and_return_failure;
            }
            write(pout, buf, 1);
            close(pout);
            continue;
        }
        case ipc_actor_start_vm_request::SETGROUPS: {
            int fds[2] = { -1, -1 };
            char buf[1];

            for (struct cmsghdr* cmsg = CMSG_FIRSTHDR(&msg) ; cmsg != NULL ;
                 cmsg = CMSG_NXTHDR(&msg, cmsg)) {
                if (cmsg->cmsg_level != SOL_SOCKET ||
                    cmsg->cmsg_type != SCM_RIGHTS) {
                    continue;
                }

                assert(sizeof(fds) >= cmsg->cmsg_len - CMSG_LEN(0));
                std::memcpy(fds, CMSG_DATA(cmsg), cmsg->cmsg_len - CMSG_LEN(0));
                break;
            }

            void* groups = NULL;
            if (request.setgroups_ngroups != 0) {
                groups = mmap(
                    /*addr=*/NULL, sizeof(gid_t) * request.setgroups_ngroups,
                    PROT_READ, MAP_SHARED, fds[1], /*offset=*/0);
                close(fds[1]);
                if (groups == MAP_FAILED)
                    goto out_cleanup_and_return_failure;
            }

            if (setgroups(
                request.setgroups_ngroups, reinterpret_cast<gid_t*>(groups)
            ) == -1) {
                goto out_cleanup_and_return_failure;
            }
            write(fds[0], buf, 1);
            close(fds[0]);
            if (groups) {
                munmap(groups, sizeof(gid_t) * request.setgroups_ngroups);
            }
            continue;
        }
        case ipc_actor_start_vm_request::SET_NO_NEW_PRIVS: {
            int pout;
            char buf[1];

            struct cmsghdr* cmsg = CMSG_FIRSTHDR(&msg);
            assert(cmsg->cmsg_level == SOL_SOCKET &&
                   cmsg->cmsg_type == SCM_RIGHTS);
            assert(sizeof(int) == cmsg->cmsg_len - CMSG_LEN(0));
            std::memcpy(&pout, CMSG_DATA(cmsg), sizeof(int));

#if BOOST_OS_LINUX
            int res = prctl(PR_SET_NO_NEW_PRIVS, 1, 0, 0, 0);
#elif BOOST_OS_BSD_FREE
            int data = PROC_NO_NEW_PRIVS_ENABLE;
            int res = procctl(P_PID, 0, PROC_NO_NEW_PRIVS_CTL, &data);
#else
            int res = -1;
#endif // BOOST_OS_LINUX

            if (res == -1)
                goto out_cleanup_and_return_failure;

            write(pout, buf, 1);
            close(pout);
            continue;
        }
#if BOOST_OS_LINUX
        case ipc_actor_start_vm_request::CAP_SET_PROC: {
            int fds[2] = { -1, -1 };
            char buf[1];

            for (struct cmsghdr* cmsg = CMSG_FIRSTHDR(&msg) ; cmsg != NULL ;
                 cmsg = CMSG_NXTHDR(&msg, cmsg)) {
                if (cmsg->cmsg_level != SOL_SOCKET ||
                    cmsg->cmsg_type != SCM_RIGHTS) {
                    continue;
                }

                assert(sizeof(fds) == cmsg->cmsg_len - CMSG_LEN(0));
                std::memcpy(fds, CMSG_DATA(cmsg), cmsg->cmsg_len - CMSG_LEN(0));
                break;
            }

            void* text = mmap(
                /*addr=*/NULL, request.cap_set_proc_mfd_size, PROT_READ,
                MAP_SHARED, fds[1], /*offset=*/0);
            close(fds[1]);
            if (text == MAP_FAILED)
                goto out_cleanup_and_return_failure;

            cap_t caps = cap_from_text(reinterpret_cast<char*>(text));
            munmap(text, request.cap_set_proc_mfd_size);
            if (caps == NULL)
                goto out_cleanup_and_return_failure;

            if (cap_set_proc(caps) == -1)
                goto out_cleanup_and_return_failure;

            write(fds[0], buf, 1);
            close(fds[0]);
            cap_free(caps);
            continue;
        }
        case ipc_actor_start_vm_request::CAP_DROP_BOUND: {
            int pout;
            char buf[1];

            struct cmsghdr* cmsg = CMSG_FIRSTHDR(&msg);
            assert(cmsg->cmsg_level == SOL_SOCKET &&
                   cmsg->cmsg_type == SCM_RIGHTS);
            assert(sizeof(int) == cmsg->cmsg_len - CMSG_LEN(0));
            std::memcpy(&pout, CMSG_DATA(cmsg), sizeof(int));


            if (cap_drop_bound(request.cap_value) == -1)
                goto out_cleanup_and_return_failure;

            write(pout, buf, 1);
            close(pout);
            continue;
        }
        case ipc_actor_start_vm_request::CAP_SET_AMBIENT: {
            int pout;
            char buf[1];

            struct cmsghdr* cmsg = CMSG_FIRSTHDR(&msg);
            assert(cmsg->cmsg_level == SOL_SOCKET &&
                   cmsg->cmsg_type == SCM_RIGHTS);
            assert(sizeof(int) == cmsg->cmsg_len - CMSG_LEN(0));
            std::memcpy(&pout, CMSG_DATA(cmsg), sizeof(int));


            if (
                cap_set_ambient(request.cap_value, request.cap_flag_value) == -1
            ) {
                goto out_cleanup_and_return_failure;
            }
            write(pout, buf, 1);
            close(pout);
            continue;
        }
        case ipc_actor_start_vm_request::CAP_RESET_AMBIENT: {
            int pout;
            char buf[1];

            struct cmsghdr* cmsg = CMSG_FIRSTHDR(&msg);
            assert(cmsg->cmsg_level == SOL_SOCKET &&
                   cmsg->cmsg_type == SCM_RIGHTS);
            assert(sizeof(int) == cmsg->cmsg_len - CMSG_LEN(0));
            std::memcpy(&pout, CMSG_DATA(cmsg), sizeof(int));

            if (cap_reset_ambient() == -1)
                goto out_cleanup_and_return_failure;

            write(pout, buf, 1);
            close(pout);
            continue;
        }
        case ipc_actor_start_vm_request::CAP_SET_SECBITS: {
            int pout;
            char buf[1];

            struct cmsghdr* cmsg = CMSG_FIRSTHDR(&msg);
            assert(cmsg->cmsg_level == SOL_SOCKET &&
                   cmsg->cmsg_type == SCM_RIGHTS);
            assert(sizeof(int) == cmsg->cmsg_len - CMSG_LEN(0));
            std::memcpy(&pout, CMSG_DATA(cmsg), sizeof(int));

            if (cap_set_secbits(request.cap_set_secbits_value) == -1)
                goto out_cleanup_and_return_failure;

            write(pout, buf, 1);
            close(pout);
            continue;
        }
        case ipc_actor_start_vm_request::SYSTEM_SECCOMP_SET_MODE_FILTER: {
            int fds[2] = { -1, -1 };
            char buf[1];

            for (struct cmsghdr* cmsg = CMSG_FIRSTHDR(&msg) ; cmsg != NULL ;
                 cmsg = CMSG_NXTHDR(&msg, cmsg)) {
                if (cmsg->cmsg_level != SOL_SOCKET ||
                    cmsg->cmsg_type != SCM_RIGHTS) {
                    continue;
                }

                assert(sizeof(fds) == cmsg->cmsg_len - CMSG_LEN(0));
                std::memcpy(fds, CMSG_DATA(cmsg), cmsg->cmsg_len - CMSG_LEN(0));
                break;
            }

            void* filter = mmap(
                /*addr=*/NULL, request.seccomp_set_mode_filter_mfd_size,
                PROT_READ, MAP_SHARED, fds[1], /*offset=*/0);
            close(fds[1]);
            if (filter == MAP_FAILED)
                goto out_cleanup_and_return_failure;

            struct sock_fprog prog;
            prog.len = request.seccomp_set_mode_filter_mfd_size /
                sizeof(struct sock_filter);
            prog.filter = reinterpret_cast<sock_filter*>(filter);

            if (prctl(PR_SET_SECCOMP, SECCOMP_MODE_FILTER, &prog) == -1)
                goto out_cleanup_and_return_failure;

            write(fds[0], buf, 1);
            close(fds[0]);
            munmap(filter, request.seccomp_set_mode_filter_mfd_size);
            continue;
        }
#endif // BOOST_OS_LINUX
        case ipc_actor_start_vm_request::CHDIR: {
            int fds[2] = { -1, -1 };
            char buf[1];

            for (struct cmsghdr* cmsg = CMSG_FIRSTHDR(&msg) ; cmsg != NULL ;
                 cmsg = CMSG_NXTHDR(&msg, cmsg)) {
                if (cmsg->cmsg_level != SOL_SOCKET ||
                    cmsg->cmsg_type != SCM_RIGHTS) {
                    continue;
                }

                assert(sizeof(fds) == cmsg->cmsg_len - CMSG_LEN(0));
                std::memcpy(fds, CMSG_DATA(cmsg), cmsg->cmsg_len - CMSG_LEN(0));
                break;
            }

            if (fchdir(fds[1]) == -1)
                goto out_cleanup_and_return_failure;

            write(fds[0], buf, 1);
            close(fds[0]);
            close(fds[1]);
            continue;
        }
        case ipc_actor_start_vm_request::CHROOT: {
            int fds[2] = { -1, -1 };
            char buf[1];

            for (struct cmsghdr* cmsg = CMSG_FIRSTHDR(&msg) ; cmsg != NULL ;
                 cmsg = CMSG_NXTHDR(&msg, cmsg)) {
                if (cmsg->cmsg_level != SOL_SOCKET ||
                    cmsg->cmsg_type != SCM_RIGHTS) {
                    continue;
                }

                assert(sizeof(fds) == cmsg->cmsg_len - CMSG_LEN(0));
                std::memcpy(fds, CMSG_DATA(cmsg), cmsg->cmsg_len - CMSG_LEN(0));
                break;
            }

            void* path = mmap(
                /*addr=*/NULL, request.chroot_mfd_size, PROT_READ, MAP_SHARED,
                fds[1], /*offset=*/0);
            close(fds[1]);
            if (path == MAP_FAILED)
                goto out_cleanup_and_return_failure;

            if (chroot(reinterpret_cast<char*>(path)) == -1)
                goto out_cleanup_and_return_failure;

            write(fds[0], buf, 1);
            close(fds[0]);
            munmap(path, request.chroot_mfd_size);
            continue;
        }
        case ipc_actor_start_vm_request::REPLACE_STDIN: {
            int fds[2] = { -1, -1 };
            char buf[1];

            for (struct cmsghdr* cmsg = CMSG_FIRSTHDR(&msg) ; cmsg != NULL ;
                 cmsg = CMSG_NXTHDR(&msg, cmsg)) {
                if (cmsg->cmsg_level != SOL_SOCKET ||
                    cmsg->cmsg_type != SCM_RIGHTS) {
                    continue;
                }

                assert(sizeof(fds) >= cmsg->cmsg_len - CMSG_LEN(0));
                std::memcpy(fds, CMSG_DATA(cmsg), cmsg->cmsg_len - CMSG_LEN(0));
                break;
            }

            if (dup2(fds[1], STDIN_FILENO) == -1)
                goto out_cleanup_and_return_failure;
            close(fds[1]);
            write(fds[0], buf, 1);
            close(fds[0]);
            continue;
        }
        case ipc_actor_start_vm_request::REPLACE_STDOUT: {
            int fds[2] = { -1, -1 };
            char buf[1];

            for (struct cmsghdr* cmsg = CMSG_FIRSTHDR(&msg) ; cmsg != NULL ;
                 cmsg = CMSG_NXTHDR(&msg, cmsg)) {
                if (cmsg->cmsg_level != SOL_SOCKET ||
                    cmsg->cmsg_type != SCM_RIGHTS) {
                    continue;
                }

                assert(sizeof(fds) >= cmsg->cmsg_len - CMSG_LEN(0));
                std::memcpy(fds, CMSG_DATA(cmsg), cmsg->cmsg_len - CMSG_LEN(0));
                break;
            }

            if (dup2(fds[1], STDOUT_FILENO) == -1)
                goto out_cleanup_and_return_failure;
            close(fds[1]);
            write(fds[0], buf, 1);
            close(fds[0]);
            continue;
        }
        case ipc_actor_start_vm_request::REPLACE_STDERR: {
            int fds[2] = { -1, -1 };
            char buf[1];

            for (struct cmsghdr* cmsg = CMSG_FIRSTHDR(&msg) ; cmsg != NULL ;
                 cmsg = CMSG_NXTHDR(&msg, cmsg)) {
                if (cmsg->cmsg_level != SOL_SOCKET ||
                    cmsg->cmsg_type != SCM_RIGHTS) {
                    continue;
                }

                assert(sizeof(fds) >= cmsg->cmsg_len - CMSG_LEN(0));
                std::memcpy(fds, CMSG_DATA(cmsg), cmsg->cmsg_len - CMSG_LEN(0));
                break;
            }

            if (dup2(fds[1], STDERR_FILENO) == -1)
                goto out_cleanup_and_return_failure;
            close(fds[1]);
            write(fds[0], buf, 1);
            close(fds[0]);
            continue;
        }
        case ipc_actor_start_vm_request::CREATE_PROCESS: {
            int fds[4] = {-1, -1, -1, -1};
            for (struct cmsghdr* cmsg = CMSG_FIRSTHDR(&msg) ; cmsg != NULL ;
                 cmsg = CMSG_NXTHDR(&msg, cmsg)) {
                if (
                    cmsg->cmsg_level != SOL_SOCKET ||
                    cmsg->cmsg_type != SCM_RIGHTS
                ) {
                    continue;
                }

                assert(sizeof(fds) >= cmsg->cmsg_len - CMSG_LEN(0));
                std::memcpy(fds, CMSG_DATA(cmsg), cmsg->cmsg_len - CMSG_LEN(0));
                break;
            }
            if (msg.msg_flags & MSG_CTRUNC) {
                for (int i = 0 ; i != 4 ; ++i) {
                    if (fds[i] == -1)
                        break;

                    close(fds[i]);
                }
                continue;
            }
            inboxfd = fds[0];
            assert(inboxfd != -1);

            switch (request.stdin_action) {
            case ipc_actor_start_vm_request::CLOSE_FD:
                proc_stdin = -1;
                break;
            case ipc_actor_start_vm_request::SHARE_PARENT:
                proc_stdin = STDIN_FILENO;
                break;
            case ipc_actor_start_vm_request::USE_PIPE:
                for (int i = 1 ; i != 4 ; ++i) {
                    if (fds[i] != -1) {
                        proc_stdin = fds[i];
                        fds[i] = -1;
                        break;
                    }
                }
                assert(proc_stdin != -1);
                break;
            }

            switch (request.stdout_action) {
            case ipc_actor_start_vm_request::CLOSE_FD:
                proc_stdout = -1;
                break;
            case ipc_actor_start_vm_request::SHARE_PARENT:
                proc_stdout = STDOUT_FILENO;
                break;
            case ipc_actor_start_vm_request::USE_PIPE:
                for (int i = 1 ; i != 4 ; ++i) {
                    if (fds[i] != -1) {
                        proc_stdout = fds[i];
                        fds[i] = -1;
                        break;
                    }
                }
                assert(proc_stdout != -1);
                break;
            }

            switch (request.stderr_action) {
            case ipc_actor_start_vm_request::CLOSE_FD:
                proc_stderr = -1;
                break;
            case ipc_actor_start_vm_request::SHARE_PARENT:
                proc_stderr = STDERR_FILENO;
                proc_stderr_has_color = request.stderr_has_color;
                break;
            case ipc_actor_start_vm_request::USE_PIPE:
                for (int i = 1 ; i != 4 ; ++i) {
                    if (fds[i] != -1) {
                        proc_stderr = fds[i];
                        fds[i] = -1;
                        break;
                    }
                }
                assert(proc_stderr != -1);
                break;
            }
            assert(fds[1] == -1);
            assert(fds[2] == -1);
            assert(fds[3] == -1);

            has_native_modules_cache = request.has_native_modules_cache;
            has_lua_hook = request.has_lua_hook;

            ipc_actor_start_vm_reply reply;
            int pidfd = -1;
#if BOOST_OS_LINUX
            request.clone_flags |= CLONE_PIDFD | SIGCHLD;
            reply.childpid = clone(
                child_main, clone_stack_address, request.clone_flags,
                /*arg=*/nullptr, &pidfd);
            reply.error = (reply.childpid == -1) ? errno : 0;
#else
            pid_t childpid = pdfork(&pidfd, request.pdfork_flags);
            if (childpid == 0) {
                return child_main(nullptr);
            }
            reply.error = (childpid == -1) ? errno : 0;
#endif // BOOST_OS_LINUX

            switch (proc_stdin) {
            case -1:
            case STDIN_FILENO:
                break;
            default:
                close(proc_stdin);
            }

            switch (proc_stdout) {
            case -1:
            case STDOUT_FILENO:
                break;
            default:
                close(proc_stdout);
            }

            switch (proc_stderr) {
            case -1:
            case STDERR_FILENO:
                break;
            default:
                close(proc_stderr);
            }

            std::memset(&msg, 0, sizeof(msg));
            iov.iov_base = &reply;
            iov.iov_len = sizeof(reply);
            msg.msg_iov = &iov;
            msg.msg_iovlen = 1;

            if (pidfd != -1) {
                msg.msg_control = cmsgbuf;
                msg.msg_controllen = CMSG_SPACE(sizeof(int));
                struct cmsghdr* cmsg = CMSG_FIRSTHDR(&msg);
                cmsg->cmsg_level = SOL_SOCKET;
                cmsg->cmsg_type = SCM_RIGHTS;
                cmsg->cmsg_len = CMSG_LEN(sizeof(int));
                std::memcpy(CMSG_DATA(cmsg), &pidfd, sizeof(int));
            }

            sendmsg(inboxfd, &msg, MSG_NOSIGNAL);
            close(inboxfd);
            if (pidfd != -1)
                close(pidfd);

            continue;
        }
        }

        // We use goto instead of BOOST_SCOPE_EXIT_ALL() here because the
        // implementation of this function should avoid C++ features that change
        // the state of the call-stack in ways C code cannot change. The reason
        // to worry about pursuing a slightly less unpredictable C call-stack
        // here is because we fork() from this call-stack and we're worried
        // about which memory is copied over to the new process and a C stack is
        // easier to reason about. Even if a C++ feature won't leak important
        // data now, it'll make the runtime call-stack harder to reason about.
        if (false) {
            static int exit_code = 0;
        out_cleanup_and_return_failure:
            exit_code = 1;
        out_cleanup:
            close_range(0, UINT_MAX, /*flags=*/0);
            while (wait(NULL) > 0);
            return exit_code;
        }
    }
}

} // namespace emilua
