local sleep = require('time').sleep

if _CONTEXT == 'main' then
    local ch = spawn_vm('.')
    sleep(0.1)
    local f = spawn(function()
        ch:send('foobar')
    end)
    sleep(0.1)
    f:cancel()
    local ok, e = pcall(function() f:join() end)
    print(f.cancellation_caught)
    error(e)
else assert(_CONTEXT == 'worker')
    require('inbox')
    spawn(function()
        collectgarbage('collect')
        sleep(0.3)
    end):detach()
end
